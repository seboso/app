class ApplicationController < ActionController::Base
  before_action :initialize_load_js

  def initialize_load_js
    @load_js = []
  end

end
